/*
Package ast

Easily converted to Pandoc, CommonMark, or Goldmark ASTs through JSON
string marshalling with ast.ConvertToPandocJSON(a AST).

*/
package ast

type Node interface{}

type Block interface{ Node }
type Inline interface{ Node }

// leaf blocks

type Sep interface{ Block }
type Heading interface{ Block }
type Raw interface{ Block }
type Fenced interface{ Block }
type Image interface{ Block } // extends
type Para interface{ Block }
type Table interface{ Block } // extends

// container blocks

type SemDiv interface{ Block } // extends
type BQuote interface{ Block }
type ListItem interface{ Block }
type List interface{ Block } // contains ListItems only

// inlines

type Esc interface{ Inline }
type Code interface{ Inline }
type Note interface{ Inline } // extends, from raw, <!-- comment -->
type Break interface{ Inline }
type Text interface{ Inline }
type Space interface{ Inline } // all ws crunched
type Math interface{ Inline }  // extends

// container inlines

type Emph interface{ Inline }
type Strong interface{ Inline }
type Link interface{ Inline }   // no titles, <> also, inline only
type Quoted interface{ Inline } // extends
type Span interface{ Inline }   // extends

/*
Omitted from Pandoc:
- Block.DefinitionList
- Block.LineBlock
- Block.Null
- Inline.Underline
- Inline.Strikeout
- Inline.Underline
- Inline.Superscript
- Inline.Subscript
- Inline.Smallcaps
- Inline.Cite
- Inline.SoftBreak
- Inline.Note

Changed from Pandoc:
- Header -> HeadingBlock
- HorizontalRule -> SepBlock
- BulletList -> ListBlock
- OrderedList -> OListBlock
- CodeBlock -> CodeBlock
- RawBlock -> RawBlock
- RawInline -> Raw

Omitted from CommonMark:
- HTMLBlock
- LinkRefDef
- Inline.Entity
- Inline.Image
- Inline.RawHTML
- Inline.SoftBreak
*/

/*

## *Just* a Title

Hello there.

----

[{"t":"Header","c":[2,["just-a-title",[],[]],[{"t":"Emph","c":[{"t":"Str","c":"Just"}]},{"t":"Space"},{"t":"Str","c":"a"},{"t":"Space"},{"t":"Str","c":"Title"}]]},{"t":"Para","c":[{"t":"Str","c":"Hello"},{"t":"Space"},{"t":"Str","c":"there."}]},{"t":"HorizontalRule"}]

[[1,2,["just-a-title"],[[1,["Just"]],"a","Title"]],[0,"Hello","there."],2]

type BlockType int
 type InlineType int

const (
	Paragraph BlockType = iota // 0
	Heading                    // 1
	Separator                  // 2

	Str InlineType     = iota // 0
	Emph
)


*/
