# EzMark, The World's Easiest and Most Compatible Markdown Flavor

* "Convention over configuration." 
* "One best way."
* "Keep it simple."

Yeah right. Even CommonMark is *anything* but easy to learn quickly. The
good news is that you really do not need to learn most of it to be
immediately productive. 

Turns out not having to parse all that extra stuff is a hell-of-a-lot
faster as well. EzMark keeps you *compatible* with everything else (GFM,
Pandoc, etc.) just *way* faster than them *and* you can parse and render
EzMark *inline* rather than having to do a double pass (once for blocks,
another for inlines).

We don't mean to be rude, but have you seen how *fat* those other
Markdowns' ASTs are? A little too much junk in those trunks.

## FAQ

### Why No "Frontmatter"? 

Because it's ***Markdown***, not something else. Having initial metadata
-- especially in YAML --- is definitely a great design in many
situations. Just not all. EzMark covers the specification of the
Markdown itself.

### Why No Dollar Math Inlines?

Because the dollar sign is used far too often for other things. Putting
all Math into a block is cleaner to read both in terms of the source.
When a lot of inline Math and other LaTeX is needed it makes more sense
to use full Pandoc.

### Why the Inflexible Parser? I Want to Extend.

We don't want you to extend it. Use another Markdown engine if you need
that. Ezmark prioritizes simplicity and efficiency over flexibility and
expandability, which is why it is the fastest Markdown flavor in the
Universe --- both for writing and parsing. (By the way, that is why you
will see an *actual* `goto` statement in the parser. It's actually much
more efficient than letting the parsing loop iterate. Core Go source
code is full of them.)
